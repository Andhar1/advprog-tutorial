package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Celery implements Veggies {

    public String toString() {
        return "Celery";
    }
}