package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class TomatoGarlicSauce implements Sauce {
    public String toString() {
        return "Tomato Garlic Sauce";
    }
}