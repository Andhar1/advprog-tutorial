package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class RolledInDough implements Dough {
    public String toString() {
        return "Rolled In Dough";
    }
}