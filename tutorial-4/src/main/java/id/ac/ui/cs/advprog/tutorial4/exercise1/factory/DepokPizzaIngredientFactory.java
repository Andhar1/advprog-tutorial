package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CotijaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.GiantClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.RolledInDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.TomatoGarlicSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Broccoli;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Celery;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Lettuce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Tomato;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Cheese createCheese() {
        return new CotijaCheese();
    }
	
	public Clams createClam() {
        return new GiantClams();
    }
	
	public Dough createDough() {
        return new RolledInDough();
    }
	
	public Sauce createSauce() {
        return new TomatoGarlicSauce();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Broccoli(), new Celery(), new Lettuce(), new Tomato()};
        return veggies;
    }

    
}
