package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;

public class WeatherData extends Observable {

    private float temperature;
    private float humidity;
    private float pressure;

    public void measurementsChanged() {
        setChanged();
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity,
                                float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float newTemperature) {
        temperature = newTemperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float newHumidity) {
        humidity = newHumidity;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float newPressure) {
        pressure = newPressure;
    }
}
