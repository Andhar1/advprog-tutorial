package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        //TODO Implement
		this.employeesList.add(employees);
    }

    public double getNetSalaries() {
        //TODO Implement
		ListIterator<Employees> employeesListIterator = employeesList.listIterator();
    	double result = 0;
    	while(employeesListIterator.hasNext()) {
    		result = result + ((Employees) employeesListIterator.next()).getSalary();
    	}
    	return result;

    }

    public List<Employees> getAllEmployees() {
        //TODO Implement
		return employeesList;
    }
}
